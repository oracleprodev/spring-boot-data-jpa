package com.software.springbootdatajpa.controller;

import com.software.springbootdatajpa.model.dao.SearchRequest;
import com.software.springbootdatajpa.model.entity.Employee;
import com.software.springbootdatajpa.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("/api/v1/employees")
@RequiredArgsConstructor
public class EmployeeController {

    private final EmployeeService employeeService;

    @PostMapping
    public ResponseEntity<?> save(@RequestBody Employee employee) {
        return ResponseEntity.ok()
                .body(employeeService.save(employee));
    }

    @GetMapping
    public ResponseEntity<?> select() {
        return ResponseEntity.ok()
                .body(employeeService.select());
    }

    @GetMapping("/select/{firstname}/{lastname}/{email}")
    public ResponseEntity<?> selectBasedFirstnameOrLastnameAndEmail(
            @PathVariable("firstname") String firstname,
            @PathVariable("lastname") String lastname,
            @PathVariable("email") String email
    ) {
        return ResponseEntity.ok().body(employeeService.selectBasedParameters(
                new HashMap<>() {{
                    put("firstname", firstname);
                    put("lastname", lastname);
                    put("email", email);
                }},
                Employee.class
        ));
    }

    @GetMapping("/select/any")
    public ResponseEntity<?> selectByAnyField(@RequestBody SearchRequest searchRequest) {
        return ResponseEntity.ok()
                .body(employeeService.selectBySearchRequest(
                        searchRequest, Employee.class
                ));
    }
}
