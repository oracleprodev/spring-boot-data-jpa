package com.software.springbootdatajpa.model.entity;

public enum Role {
    USER,
    ADMIN
}
