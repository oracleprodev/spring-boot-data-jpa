package com.software.springbootdatajpa.model.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Mission {

    @Id
    @GeneratedValue
    private long id;

    private String name;

    private Integer duration;

    @ManyToOne
    private Employee employee;
}
