package com.software.springbootdatajpa.service;

import com.software.springbootdatajpa.model.dao.SearchRequest;

import java.util.List;
import java.util.Map;

public interface GeneralService<T> {

    List<T> select();

    List<T> selectBasedParameters(Map<String, Object> parameters, Class<T> entityType);

    List<T> selectBySearchRequest(SearchRequest searchRequest, Class<T> ModelType);

    T save(T t);
}
