package com.software.springbootdatajpa.service;

import com.software.springbootdatajpa.dao.EmployeeDAO;
import com.software.springbootdatajpa.dao.GenericEmployeeDAO;
import com.software.springbootdatajpa.model.dao.SearchRequest;
import com.software.springbootdatajpa.model.entity.Employee;
import com.software.springbootdatajpa.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class EmployeeService implements GeneralService<Employee> {

    private final EmployeeRepository employeeRepository;

//    below is simple DAO class
//    private final EmployeeDAO employeeDAO;

    //    Below is Generic Employee DAO
    private final GenericEmployeeDAO<Employee> employeeDAO;

    @Override
    public List<Employee> select() {
        return employeeRepository.findAll();
    }

    @Override
    public List<Employee> selectBasedParameters(
            Map<String, Object> parameters,
            Class<Employee> entityType) {

        return employeeDAO.selectBySimpleCriteria(parameters, entityType);
    }

    @Override
    public List<Employee> selectBySearchRequest(SearchRequest searchRequest, Class<Employee> ModelType) {
        return employeeDAO.selectBySearchRequest(searchRequest,ModelType);
    }

    @Override
    public Employee save(Employee employee) {
        return employeeRepository.save(employee);
    }


}
