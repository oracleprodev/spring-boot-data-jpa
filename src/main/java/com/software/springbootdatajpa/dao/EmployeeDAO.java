package com.software.springbootdatajpa.dao;

import com.software.springbootdatajpa.model.dao.SearchRequest;
import com.software.springbootdatajpa.model.entity.Employee;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
@RequiredArgsConstructor
public class EmployeeDAO {

    private final EntityManager entityManager;

    public List<Employee> selectBySimpleCriteria(Map<String, Object> parameters, Class<Employee> clazz) {

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Employee> criteriaQuery = criteriaBuilder.createQuery(clazz);

        // select * from employee
        Root<Employee> root = criteriaQuery.from(Employee.class);

        //Prepare WhereClause
        Predicate firstnamePredicate = criteriaBuilder.
                like(root.get("firstname"), "%" + parameters.get("firstname") + "%");
        Predicate lastnamePredicate = criteriaBuilder.
                like(root.get("lastname"), "%" + parameters.get("lastname") + "%");
        Predicate emailPredicate = criteriaBuilder.
                like(root.get("email"), "%" + parameters.get("email") + "%");

        Predicate orPredicate = criteriaBuilder.
                or(firstnamePredicate, lastnamePredicate);

        Predicate andPredicate = criteriaBuilder.and(emailPredicate);

        // select * from Employee
        // where firstname like '%kim%' or lastname like '% kenwood%'
        // and email like '%my@mail.com%'

        criteriaQuery.where(orPredicate, andPredicate);
        TypedQuery<Employee> typedQuery = entityManager.createQuery(criteriaQuery);

        return typedQuery.getResultList();

    }

    public List<Employee> selectBySearchRequest(
            SearchRequest employeeSearchRequest, Class<Employee> clazz) {

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Employee> criteriaQuery = criteriaBuilder.createQuery(clazz);
        List<Predicate> predicates = new ArrayList<>();

        // select * from employee
        Root<Employee> root = criteriaQuery.from(Employee.class);

        //Prepare WhereClause
        if (employeeSearchRequest.getFirstname() != null) {
            Predicate firstnamePredicate = criteriaBuilder.
                    like(root.get("firstname"), "%" + employeeSearchRequest.getFirstname() + "%");
            predicates.add(firstnamePredicate);
        }

        if (employeeSearchRequest.getLastname() != null) {
            Predicate lastnamePredicate = criteriaBuilder.
                    like(root.get("lastname"), "%" + employeeSearchRequest.getLastname() + "%");
            predicates.add(lastnamePredicate);
        }

        if (employeeSearchRequest.getEmail() != null) {
            Predicate emailPredicate = criteriaBuilder.
                    like(root.get("firstname"), "%" + employeeSearchRequest.getEmail() + "%");
            predicates.add(emailPredicate);
        }

        // select * from Employee
        // where firstname like '%kim%' or lastname like '% kenwood%'
        // or email like '%my@mail.com%'

        criteriaQuery.where(
                criteriaBuilder.or(predicates.toArray(new Predicate[0]))
        );
        TypedQuery<Employee> typedQuery = entityManager.createQuery(criteriaQuery);

        return typedQuery.getResultList();

    }
}
