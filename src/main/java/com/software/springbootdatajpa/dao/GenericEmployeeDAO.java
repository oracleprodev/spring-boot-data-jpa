package com.software.springbootdatajpa.dao;

import com.software.springbootdatajpa.model.dao.SearchRequest;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Repository
@RequiredArgsConstructor
public class GenericEmployeeDAO<T> {

    private final EntityManager entityManager;

    public List<T> selectBySimpleCriteria(Map<String, Object> parameters, Class<T> entityType) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(entityType);

        Root<T> root = criteriaQuery.from(entityType);

        Predicate[] predicates = parameters.entrySet().stream()
                .map(entry -> criteriaBuilder.like(
                        root.get(entry.getKey()), "%" + entry.getValue() + "%"))
                .toArray(Predicate[]::new);

        criteriaQuery.where(criteriaBuilder.and(predicates));

        TypedQuery<T> typedQuery = entityManager.createQuery(criteriaQuery);

        return typedQuery.getResultList();
    }

    public List<T> selectBySearchRequest(SearchRequest searchRequest, Class<T> clazz) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(clazz);
        Root<T> root = criteriaQuery.from(clazz);

        List<Predicate> predicates = new ArrayList<>(getPredicates(searchRequest, root, criteriaBuilder));

        criteriaQuery.where(criteriaBuilder.or(predicates.toArray(new Predicate[0])));
        TypedQuery<T> typedQuery = entityManager.createQuery(criteriaQuery);

        return typedQuery.getResultList();
    }

    private List<Predicate> getPredicates(SearchRequest searchRequest, Root<T> root, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();

        if (Objects.nonNull(searchRequest.getFirstname())) {
            predicates.add(criteriaBuilder.like(root.get("firstname"), "%" + searchRequest.getFirstname() + "%"));
        }

        if (Objects.nonNull(searchRequest.getLastname())) {
            predicates.add(criteriaBuilder.like(root.get("lastname"), "%" + searchRequest.getLastname() + "%"));
        }

        if (Objects.nonNull(searchRequest.getEmail())) {
            predicates.add(criteriaBuilder.like(root.get("email"), "%" + searchRequest.getEmail() + "%"));
        }

        return predicates;
    }
}
